package org.bitbucket.openisoj;

public interface IMessage {
	public byte[] toMsg() throws Exception;

	public int unpack(byte[] msg, int offset) throws Exception;
}
